<?php
namespace Auth\DAL;

use Zend\Http\Client;
use Zend\Http\Request;
use Zend\Json\Json;

class ImoServices
{

    const URL_Login = '/Token';

    const URL_Values = '/api/values';

    public static function Login(Credenciais $credenciais)
    {
        $client = new Client('https://' . $_SESSION['server'] . self::URL_Login);
        
        $client->setMethod(Request::METHOD_POST);
        $params = 'grant_type=password&username=' . $credenciais->email . '&password=' . $credenciais->password;
        
        $len = strlen($params);
        
        $client->setHeaders(array(
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Content-Length' => $len
        ));
        
        $client->setOptions([
            'sslverifypeer' => false
        ]);
        $client->setRawBody($params);
        
        $response = $client->send();
        
        $body = Json::decode($response->getBody());
        
        if (! empty($body->access_token)) {
            $_SESSION['access_token'] = $body->access_token;
            $_SESSION['email'] = $credenciais->email;
            
            return true;
        } else
            return false;
    }

    public static function Logout()
    {
        $_SESSION['email'] = null;
        $_SESSION['access_token'] = null;
    }

    public static function getValues()
    {
        $client = new Client('https://' . $_SESSION['server'] . self::URL_Values);
        
        $client->setMethod(Request::METHOD_GET);
        
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        
        $client->setHeaders(array(
            'Authorization' => $bearer_token
        ));
        
        $client->setOptions([
            'sslverifypeer' => false
        ]);
        
        $response = $client->send();
        
        $body = $response->getBody();
        
        return $body;
    }
    
    // -> /api/allpoi
    public static function getAllPOI()
    {
        $client = new Client('https://' . $_SESSION['server'] . '/api/allpoi');
        
        $client->setMethod(Request::METHOD_GET);
        
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        
        $client->setHeaders(array(
            'Authorization' => $bearer_token
        ));
        
        $client->setOptions([
            'sslverifypeer' => false
        ]);
        
        $response = $client->send();
        
        $body = $response->getBody();
        
        return $body;
    }
    
    // -> api/getTemperatura?idLocal={idLocal={idLocal}&data_i={data_i}&data_f={data_f}
    public static function getTemperatura($idLocal, $data_i, $data_f)
    {
        $addr = 'https://' . $_SESSION['server'] . '/api/poi/' . $idLocal . '/data1/' . $data_i . '/data2/' . $data_f . '/temperatura';
        //echo $addr;
        $client = new Client($addr);
        
        $client->setMethod(Request::METHOD_GET);
        
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        
        $client->setHeaders(array(
            'Authorization' => $bearer_token
        ));
        
        $client->setOptions([
            'sslverifypeer' => false
        ]);
        
        $response = $client->send();
        
        $body = $response->getBody();
        
        return $body;
    }
    
    // -> api/poi/{idLocal}/data1/{data_i}/data2/{data_f}/vento
    public static function getVento($idLocal, $data_i, $data_f)
    {
        $client = new Client('https://' . $_SESSION['server'] . '/api/poi/' . $idLocal . '/data1/' . $data_i . '/data2/' . $data_f . '/vento');
        
        $client->setMethod(Request::METHOD_GET);
        
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        
        $client->setHeaders(array(
            'Authorization' => $bearer_token
        ));
        
        $client->setOptions([
            'sslverifypeer' => false
        ]);
        
        $response = $client->send();
        
        $body = $response->getBody();
        
        return $body;
    }
    
    // -> api/poi/{idLocal}/data1/{data_i}/data2/{data_f}/humidade
    public static function getHumidade($idLocal, $data_i, $data_f)
    {
        $client = new Client('https://' . $_SESSION['server'] . '/api/poi/' . $idLocal . '/data1/' . $data_i . '/data2/' . $data_f . '/humidade');
        
        $client->setMethod(Request::METHOD_GET);
        
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        
        $client->setHeaders(array(
            'Authorization' => $bearer_token
        ));
        
        $client->setOptions([
            'sslverifypeer' => false
        ]);
        
        $response = $client->send();
        
        $body = $response->getBody();
        
        return $body;
    }
}

?>