<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Auth for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Auth\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Auth\DAL\ImoServices;
use Auth\Form\LoginForm;
use Auth\Form\RegistoForm;
use Auth\DAL\Credenciais;
use Exception;
use Auth\Model\Turista;

class AuthController extends AbstractActionController
{

    public function __construct()
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
    }

    /**
     * user: teste@isep.pt
     * pass: Qwerty1.
     */
    public function loginAction()
    {
        $message='';
        $form = new LoginForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            if ($this->checkLogin($request)) {
                $this->cancelaLogin();
                return $this->redirect()->toRoute('percurso', array(
                    'action' => 'index'
                ));
            }else{
                $message = 'As Credenciais introduzidas sao invalidas !';
            }
        }
        return (array(
            'form' => $form,
            'message'=>$message
        ));
    }
    
    public function cancelaLogin(){
        $_SESSION['server'] = 'wvm114.dei.isep.ipp.pt/Cancela';
        ImoServices::Logout();
        $credenciais = new Credenciais();
        $credenciais->exchangeArray(array(
            'email' => 'teste@isep.pt',
            'password' => 'Qwerty1.'
        ));
        
        while (intval($_SESSION['access_token']) == 0) {
            try {
                ImoServices::Login($credenciais);
            } catch (Exception $e) {
                // echo failed token (ocorreu timeout)
            }
        }
    }

    public function checkLogin($request)
    {
        $id = $this->getTuristaTable()->validateLogin($request->getPost('email'), $request->getPost('password'));
        if (intval($id) > 0) {
            $_SESSION['turista'] = $id;
            return true;
        }
        return false;
    }
    
    public function registoAction()
    {
        $message = '';
        $form = new RegistoForm();
        $form->get('submit')->setValue('Registar');
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $turista = new Turista();
            $form->setInputFilter($turista->getInputFilter());
            $data = $request->getPost();
            $data['password'] = md5($data['password']);
            $form->setData($data);
            
            if ($form->isValid()) {
                $turista->exchangeArray($form->getData());
                $this->getTuristaTable()->saveTurista($turista);
                
                return $this->redirect()->toRoute('percurso', array(
                    'action' => 'index'
                ));
            }
        }
        return array(
            'form' => $form,
            'message'=>$message
        );
    }

    public function tokenAction()
    {
        return array(
            'session' => $_SESSION['access_token']
        );
    }
    
    public function turistasAction()
    {
        return array(
            'turistas' => $this->getTuristaTable()->fetchAll()
        );
    }

    public function logoutAction()
    {
        ImoServices::Logout();
        unset($_SESSION['turista']);
        return $this->redirect()->toRoute('auth', array(
            'controller' => 'auth',
            'action' => 'login'
        ));
    }

    public function getTuristaTable()
    {
        $sm = $this->getServiceLocator();
        $turistaTable = $sm->get('Auth\Model\TuristaTable');
        
        return $turistaTable;
    }
}
