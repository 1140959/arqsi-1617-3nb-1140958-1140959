<?php
namespace Auth\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;

class Turista
{
    public $id;
    public $nome;
    public $nacionalidade;
    public $password;
    public $email;
    
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->id     = (!empty($data['id'])) ? $data['id'] : null;
        $this->nome  = (!empty($data['nome'])) ? $data['nome'] : null;
        $this->nacionalidade  = (!empty($data['nacionalidade'])) ? $data['nacionalidade'] : null;
        $this->password  = (!empty($data['password'])) ? $data['password'] : null;
        $this->email = (!empty($data['email'])) ? $data['email'] : null;
    }
    
    // Add content to these methods:
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }
    
    public function getInputFilter()
    {
        if (! $this->inputFilter) {
            $inputFilter = new InputFilter();
    
            $inputFilter->add(array(
                'name' => 'id',
                'required' => false
            ));
    
            $inputFilter->add(array(
                'name' => 'nome',
                'required' => true
            ));
    
            $inputFilter->add(array(
                'name' => 'nacionalidade',
                'required' => true
            ));
    
            $inputFilter->add(array(
                'name' => 'password',
                'required' => true
            ));
            
            $inputFilter->add(array(
                'name' => 'email',
                'required' => true
            ));
    
            $this->inputFilter = $inputFilter;
        }
    
        return $this->inputFilter;
    }
}

