<?php
namespace Auth\Form;

use Zend\Form\Form;

class RegistoForm extends Form
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('credenciais');

        $this->add(array(
            'name' => 'email',
            'type' => 'Text',
            'options' => array(
                //'label' => 'Email',
            ),
            'attributes' => array(
                'size' => '40'
            )
        ));
        $this->add(array(
            'name' => 'nome',
            'type' => 'Text',
            'options' => array(
                //'label' => 'Email',
            ),
            'attributes' => array(
                'size' => '40'
            )
        ));
        $this->add(array(
            'name' => 'nacionalidade',
            'type' => 'Text',
            'options' => array(
                //'label' => 'Email',
            ),
            'attributes' => array(
                'size' => '40'
            )
        ));
        $this->add(array(
            'name' => 'password',
            'type' => 'Password',
            'options' => array(
                //'label' => 'Password',
            ),
            'attributes' => array(
                'size' => '40'
            )
        ));
        $this->add(array(
            'name' => 'password2',
            'type' => 'Password',
            'options' => array(
                //'label' => 'Password',
            ),
            'attributes' => array(
                'size' => '40'
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
    }
}
?>