<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;

/**
 * BaseController
 *
 * @author
 *
 * @version
 *
 */
class BaseController extends AbstractActionController
{

    public function __construct()
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
    }

    public function onDispatch(\Zend\Mvc\MvcEvent $e)
    {
        if (! isset($_SESSION['turista']))
            return $this->redirect()->toRoute('auth', array(
                'action' => 'login'
            ));
        return parent::onDispatch($e);
    }
    
    // Tabelas Base de Dados
    public function getPercursoTable()
    {
        $sm = $this->getServiceLocator();
        $turistaTable = $sm->get('Percurso\Model\PercursoTable');
    
        return $turistaTable;
    }
    
    public function getPontoPassagemTable()
    {
        $sm = $this->getServiceLocator();
        $pontoPassagemTable = $sm->get('Percurso\Model\PontoPassagemTable');
    
        return $pontoPassagemTable;
    }
    
    public function getTuristaTable()
    {
        $sm = $this->getServiceLocator();
        $turistaTable = $sm->get('Auth\Model\TuristaTable');
    
        return $turistaTable;
    }
}