<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'Percurso\Controller\Percurso' => 'Percurso\Controller\PercursoController',
        ),
    ),

    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'percurso' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/percurso[/:action][/:id][/:param1][/:param2][/:param3][/:param4][/:param5]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Percurso\Controller\Percurso',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),

    'view_manager' => array(
        'template_path_stack' => array(
            'percurso' => __DIR__ . '/../view',
        ),
    ),
);
