<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Percurso for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Percurso\Controller;

use Zend\View\Model\ViewModel;
use Auth\DAL\ImoServices;
use Zend\Json\Json;
use Zend\Form\Form;
use Percurso\Form\PercursoForm;
use Percurso\Model\Percurso;
use Percurso\Model\PontoPassagem;
use Application\Controller\BaseController;

class PercursoController extends BaseController
{   
    public function indexAction()
    {
        return new ViewModel(array(
            'percursos' => $this->getPercursoTable()->all()
        ));
    }
    
    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('percurso', array(
                'action' => 'index'
            ));
        }
         
        try {
            $percurso = $this->getPercursoTable()->getPercurso($id);
        }
        catch (\Exception $ex) {
            return $this->redirect()->toRoute('percurso', array(
                'action' => 'edit'
            ));
        }
         
        $form  = new PercursoForm();
        $form->bind($percurso);
        $form->get('submit')->setAttribute('value', 'Edit');
         
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($percurso->getInputFilter());
            $data = $request->getPost();
            $data['id_turista'] =  $_SESSION['turista'];
            $form->setData($data);
             
            if ($form->isValid()) {
                $this->getPercursoTable()->savePercurso($percurso);
                 
                return $this->redirect()->toRoute('percurso');
            }
        }
         
        return array(
            'id' => $id,
            'form' => $form,
        );
    }

    public function deleteAction()
    {
        $id = $this->params('id');
        if (! $id) {
            return $this->redirect()->toRoute('percurso');
        }
         
        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');
             
            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->getPercursoTable()->deletePercurso($id);
            }
             
            // Redirect to list of albums
            return $this->redirect()->toRoute('percurso');
        }
         
        return array(
            'id' => $id,
            'percurso' => $this->getPercursoTable()->getPercurso($id)
        );
    }

    public function novopontoAction()
    {
        $json = ImoServices::getAllPOI();
        return new ViewModel(array(
            'pois' => Json::decode($json),
            'percurso' => $this->params('id')
        ));
    }

    public function pontospassagemAction()
    {
        $id_percurso = $this->params('id');
        return new ViewModel(array(
            'pontospassagem' => $this->getPontoPassagemTable()->getPontoPassagemPercurso($id_percurso),
            'owner' => $this->getPercursoTable()->getOwner($id_percurso),
            'percurso'=>$id_percurso
        ));
    }
    
    public function novoAction()
    {
        $form = new PercursoForm();
        $form->get('submit')->setValue('Criar');
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $percurso = new Percurso();
            $form->setInputFilter($percurso->getInputFilter());
            $data = $request->getPost();
            $data['id_turista'] = $_SESSION['turista'];
            $form->setData($data);
            
            if ($form->isValid()) {
                $percurso->exchangeArray($form->getData());
                $this->getPercursoTable()->savePercurso($percurso);
                
                // Redirect to list of albums
                return $this->redirect()->toRoute('percurso', array(
                    'action' => 'index'
                ));
            }
        }
        return array(
            'form' => $form
        );
    }

    public function addAction()
    {
        $id = $this->params('id');
        $ponto = new PontoPassagem();
        $this->getPontoPassagemTable()->savePontoPassagem($ponto);
        return $this->redirect()->toRoute('percurso', array(
            'action' => 'novoponto',
            'id' => $id
        ));
    }
    
    public function consultaAction()
    {
        $id = $this->params('id');
   
        $date = str_replace('-', '/', date("Y-m-d"));
        $dia1 = date('Y-m-d', strtotime($date . "-1 days"));
        $dia2 = date('Y-m-d', strtotime($date . "-2 days"));
        
        $humidade = ImoServices::getHumidade($id, $dia1, $dia2);
        $vento = ImoServices::getVento($id, $dia1, $dia2);
        $temp = ImoServices::getTemperatura($id, $dia1, $dia2);
        
        return new ViewModel(array(
            'humidade' => Json::decode($humidade),
            'vento' => Json::decode($vento),
            'temp' => Json::decode($temp),
            'percurso' =>$this->params('param1')
        ));
    }
    /*
     'id' => $percurso,
                'param1' => $p->Nome,
                'param2' => $p->Descricao,
                'param3' => $p->GPS_Lat,
                'param4' => $p->GPS_Long,
                'param5' => $p->id
     */
    public function addpoiAction()
    {
        $ponto = new PontoPassagem();
        $ponto->id_percurso = $this->params('id');
        $ponto->nome = $this->params('param1');
        $ponto->descricao = $this->params('param2');
        $ponto->gps_lat = $this->params('param3');
        $ponto->gps_long = $this->params('param4');
        $ponto->id_poi = $this->params('param5');
        
        $this->getPontoPassagemTable()->savePontoPassagem($ponto);
        
        return $this->redirect()->toRoute('percurso', array(
            'action' => 'pontospassagem',
            'id'=>$this->params('id')
        ));
    }
    
    public function deletepoiAction()
    {
        $id_percurso = $this->params('id');
        $ponto = $this->params('param1');
        
        $this->getPontoPassagemTable()->deletePontoPassagem($ponto);
        
        return $this->redirect()->toRoute('percurso', array(
            'action' => 'pontospassagem',
            'id'=>$id_percurso
        ));
    }
}
