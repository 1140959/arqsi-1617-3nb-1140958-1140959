<?php
namespace Percurso\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class PercursoForm extends Form
{

    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('server');
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Date',
            'name' => 'data',
            'options' => array(
                //'label' => 'Data '
            ),
            'attributes' => array(
                'min' => '2012-01-01',
                'max' => '2020-01-01',
                'step' => '1'
            )
        )); // days; default step interval is 1 day
        
        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden'
        ));
        
        $this->add(array(
            'name' => 'id_turista',
            'type' => 'Hidden'
        ));
        
        $this->add(array(
            'name' => 'descricao',
            'type' => 'Text',
            'options' => array(
                //'label' => 'Descricao'
            )
        ));
        
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton'
            )
        ));
    }
}
?>