<?php
namespace Percurso\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilterAwareInterface;

class Percurso implements InputFilterAwareInterface
{

    public $id;
    public $descricao;
    public $data;
    public $id_turista;
    public $nome;
    
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->id = (! empty($data['id'])) ? $data['id'] : null;
        $this->descricao = (! empty($data['descricao'])) ? $data['descricao'] : null;
        $this->data = (! empty($data['data'])) ? $data['data'] : null;
        $this->id_turista = (! empty($data['id_turista'])) ? $data['id_turista'] : null;
        $this->nome = (! empty($data['nome'])) ? $data['nome'] : null;
    }
    

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    
    // Add content to these methods:
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (! $this->inputFilter) {
            $inputFilter = new InputFilter();
            
            $inputFilter->add(array(
                'name' => 'id',
                'required' => false
            ));
            
            $inputFilter->add(array(
                'name' => 'id_turista',
                'required' => true
            ));
            
            $inputFilter->add(array(
                'name' => 'descricao',
                'required' => true
            ));
            
            $inputFilter->add(array(
                'name' => 'data',
                'required' => true
            ));
            
            $this->inputFilter = $inputFilter;
        }
        
        return $this->inputFilter;
    }
}

