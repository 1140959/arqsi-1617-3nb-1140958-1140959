<?php
namespace Percurso\Model;

use Zend\Db\TableGateway\TableGateway;

class PercursoTable
{
    protected $tableGateway;
    
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
    
    public function all()
    {
        $sqlSelect = $this->tableGateway->getSql()->select();
        $sqlSelect->columns(array('descricao','data','id','id_turista'));
        $sqlSelect->join('turista', 'turista.id = percurso.id_turista', array('nome'), 'inner');
        
        $resultSet = $this->tableGateway->selectWith($sqlSelect);
        return $resultSet;
    }
    
    public function getPercurso($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }
    
    public function savePercurso(Percurso $percurso)
    {
        $data = array(
            'descricao' => $percurso->descricao,
            'data'  => $percurso->data,
            'id_turista'  => $percurso->id_turista,
        );
    
        $id = (int) $percurso->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getPercurso($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Percurso id does not exist');
            }
        }
    }
    
    public function deletePercurso($id)
    {
        $this->tableGateway->delete(array('id' => (int) $id));
    }
    
    public function getOwner($id)
    {
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception('Percurso id does not exist');
        }
        return $row->id_turista;
    }
}

