<?php
namespace Percurso\Model;

use Zend\Db\TableGateway\TableGateway;

class PontoPassagemTable
{
    protected $tableGateway;
    
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
    
    public function getPontoPassagem($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }
    
    public function getPontoPassagemPercurso($id)
    {
        $id  = (int) $id;
        $resultSet = $this->tableGateway->select(array('id_percurso' => $id));
        return $resultSet;
    }
    
    public function savePontoPassagem(PontoPassagem $pontopassagem)
    {
        $data = array(
            'descricao' => $pontopassagem->descricao,
            'nome'  => $pontopassagem->nome,
            'gps_lat'  => $pontopassagem->gps_lat,
            'gps_long'  => $pontopassagem->gps_long,
            'id_percurso'  => $pontopassagem->id_percurso,
            'id_poi'=> $pontopassagem->id_poi,
        );
    
        $id = (int) $pontopassagem->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getPontoPassagem($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('PontoPassagem id does not exist');
            }
        }
    }
    
    public function deletePontoPassagem($id)
    {
        $this->tableGateway->delete(array('id' => (int) $id));
    }
}

