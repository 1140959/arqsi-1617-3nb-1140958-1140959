<?php
namespace Percurso\Model;

class PontoPassagem
{
    public $id;
    public $nome;
    public $descricao;
    public $gps_lat;
    public $gps_long;
    public $id_percurso;
    public $id_poi;
    
    public function exchangeArray($data)
    {
        $this->id     = (!empty($data['id'])) ? $data['id'] : null;
        $this->nome     = (!empty($data['nome'])) ? $data['nome'] : null;
        $this->descricao     = (!empty($data['descricao'])) ? $data['descricao'] : null;
        $this->gps_lat  = (!empty($data['gps_lat'])) ? $data['gps_lat'] : null;
        $this->gps_long  = (!empty($data['gps_long'])) ? $data['gps_long'] : null;
        $this->id_percurso  = (!empty($data['id_percurso'])) ? $data['id_percurso'] : null;
        $this->id_poi  = (!empty($data['id_poi'])) ? $data['id_poi'] : null;
    }
}

