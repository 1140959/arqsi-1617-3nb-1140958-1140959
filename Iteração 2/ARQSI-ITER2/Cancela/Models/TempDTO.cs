﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Cancela.Models
{
    public class TempDTO
    {
        public double temp { get; set; }
        public DateTime data { get; set; }
    }
}