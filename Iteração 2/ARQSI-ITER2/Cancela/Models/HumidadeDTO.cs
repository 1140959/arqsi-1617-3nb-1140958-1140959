﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cancela.Models
{
    public class HumidadeDTO
    {
        public double humidade { get; set; }
        public DateTime data { get; set; }
    }
}