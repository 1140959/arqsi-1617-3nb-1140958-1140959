﻿using ClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CancelaAPI.Models
{
    public class MeteoDTO
    {
        public int ID { get; set; }
        public Local Local { get; set; }
    }

    public class MeteoDetailsDTO
    {
        public int ID { get; set; }
        public LocalDTO Local { get; set; }
        public DateTime dataLeitura { get; set; }
        public DateTime horaLeitura { get; set; }
        public double temp { get; set; }
        public double vento { get; set; }
        public double humidade { get; set; }
        public double pressao { get; set; }
        public double NO { get; set; }
        public double NO2 { get; set; }
        public double CO2 { get; set; }
    }
}