﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cancela.Models
{
    public class POIDTO
    {
        public int ID { get; set; }
        public string Nome { get; set; }       
        public string Descricao { get; set; }
        public string GPS_Lat { get; set; }
        public string GPS_Long { get; set; }
        public string Created { get; set; }
    }

}