﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CancelaAPI.Models
{

    public class LocalDTO
    { 
        public int ID { get; set; }
        public string Nome { get; set; }
    }

    public class LocalDetailsDTO
    {
        public int ID { get; set; }
        public string GPSLat { get; set; }
        public string GPSLong { get; set; }
        public string Nome { get; set; }
    }
}