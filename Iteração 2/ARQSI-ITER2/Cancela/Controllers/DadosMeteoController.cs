﻿using CancelaAPI.Models;
using Datum;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Cancela.Controllers
{
    [Authorize(Roles = "Editor")]
    [EnableCors(origins: "https://localhost:50959", headers: "*", methods: "*")]
    public class DadosMeteoController : ApiController
    {
        private DatumContext db = new DatumContext();
        
        public async Task<List<MeteoDetailsDTO>> GetPesquisa([FromUri]int id_poi)
        {
            var poi = db.POI.Find(id_poi);
            
            var mets = await db.Meteo.Where(m => m.LocalID == poi.LocalID)
              .Select(m => new MeteoDetailsDTO
              {
                  ID = m.ID,
                  dataLeitura = m.dataLeitura,
                  horaLeitura = m.horaLeitura,
                  temp = m.temp,
                  humidade = m.humidade,
                  vento = m.vento,
                  pressao = m.pressao,
                  CO2 = m.CO2,
                  NO = m.NO,
                  NO2 = m.NO2
              })
            .ToListAsync();

            return mets;
        }
    }
}
