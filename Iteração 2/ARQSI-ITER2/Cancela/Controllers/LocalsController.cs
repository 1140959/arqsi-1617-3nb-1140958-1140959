﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.Models;
using Datum;
using System.Threading.Tasks;
using CancelaAPI.Models;
using System.Web.Http.Cors;

namespace Cancela.Controllers
{
    [Authorize]
    [EnableCors(origins: "https://localhost:50959", headers:"*", methods:"*")]
    public class LocalsController : ApiController
    {
        private DatumContext db = new DatumContext();

        //Get de Locals
        public IEnumerable<LocalDTO> GetLocals()
        {
            var local = from b in db.Locais
                        select new LocalDTO()
                        {
                            ID = b.ID,
                            Nome = b.Nome,
                        };

            return local;
        }

        // GET api/Locals/5
        [ResponseType(typeof(LocalDetailsDTO))]
        public async Task<IHttpActionResult> GetLocal(int id)
        {
            var local = await db.Locais.Select(b =>
                new LocalDetailsDTO()
                {
                    ID = b.ID,
                    Nome=b.Nome

                }).SingleOrDefaultAsync(b => b.ID == id);
            if (local == null)
            {
                return NotFound();
            }

            return Ok(local);
        }
        // PUT: api/Locals/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutLocal(int id, Local local)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != local.ID)
            {
                return BadRequest();
            }

            db.Entry(local).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LocalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Locals
        [ResponseType(typeof(Local))]
        public async Task<IHttpActionResult> PostLocal(Local local)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Locais.Add(local);
            await db.SaveChangesAsync();

            // New code:
            // Load author name
            //db.Entry(local).Reference(x => x.Author).Load();

            var dto = new LocalDTO()
            {
                ID = local.ID,
                Nome = local.Nome,
                //AuthorName = local.Author.Name
            };

            return CreatedAtRoute("DefaultApi", new { id = local.ID }, dto);
        }

        // DELETE: api/Locals/5
        [ResponseType(typeof(Local))]
        public IHttpActionResult DeleteLocal(int id)
        {
            Local local = db.Locais.Find(id);
            if (local == null)
            {
                return NotFound();
            }

            db.Locais.Remove(local);
            db.SaveChanges();

            return Ok(local);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LocalExists(int id)
        {
            return db.Locais.Count(e => e.ID == id) > 0;
        }
    }
}