﻿using System;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.Models;
using Datum;
using System.Web.Http.Cors;
using Cancela.Models;

namespace Cancela.Controllers
{
    //[Authorize]
    [EnableCors(origins: "https://localhost:50959", headers: "*", methods: "*")]
    public class PointOfInterestsController : ApiController
    {
        private DatumContext db = new DatumContext();

        //[Route("api/getTemperatura?idLocal={idLocal={idLocal}&data_i={data_i}&data_f={data_f}")]
        [Route("api/poi/{id_poi}/data1/{data_i}/data2/{data_f}/temperatura")]
        [HttpGet]
        public IQueryable<TempDTO> getTemperatura(int id_poi, DateTime data_i, DateTime data_f)
        {
            var mets = from meteo in db.Meteo
                       join poi in db.POI on meteo.LocalID equals poi.LocalID
                       where poi.ID == id_poi
                       &&(meteo.dataLeitura == data_i || meteo.dataLeitura == data_f)
                       select new TempDTO()
                       {
                          temp = meteo.temp,
                          data = meteo.dataLeitura 
                       };
            return mets.AsQueryable();
        }

        /*
         var poi = db.POI.Find(id_poi);
            var lstMeteo = db.Meteo.Where(m => m.LocalID == poi.LocalID && m.dataLeitura >= data_i && m.dataLeitura <= data_f)
                                   .GroupBy(m => m.dataLeitura)
                                   .Select(g =>
                                   new TempDTO
                                   {
                                       temp = g.Average(m => m.temp),
                                       data = g.Key
                                   });
            return lstMeteo;
    
        */


        //[Route("api/getTemperatura?idLocal={idLocal={idLocal}&data_i={data_i}&data_f={data_f}")]
        [Route("api/poi/{id_poi}/data1/{data_i}/data2/{data_f}/vento")]
        [HttpGet]
        public IQueryable<VentoDTO> getVento(int id_poi, DateTime data_i, DateTime data_f)
        {
            var mets = from meteo in db.Meteo
                       join poi in db.POI on meteo.LocalID equals poi.LocalID
                       where poi.ID == id_poi
                       && (meteo.dataLeitura == data_i || meteo.dataLeitura == data_f)
                       select new VentoDTO()
                       {
                           vento = meteo.vento,
                           data = meteo.dataLeitura
                       };
            return mets.AsQueryable();
        }

        /*
          var poi = db.POI.Find(id_poi);
            var lstMeteo = db.Meteo.Where(m => m.LocalID == poi.LocalID && m.dataLeitura >= data_i && m.dataLeitura <= data_f)
                                   .GroupBy(m => m.dataLeitura)
                                   .Select(g =>
                                   new VentoDTO
                                   {
                                       vento = g.Average(m => m.vento),
                                       data = g.Key
                                   });
            return lstMeteo;
        */

        //[Route("api/getTemperatura?idLocal={idLocal={idLocal}&data_i={data_i}&data_f={data_f}")]
        [Route("api/poi/{id_poi}/data1/{data_i}/data2/{data_f}/humidade")]
        [HttpGet]
        public IQueryable<HumidadeDTO> getHumidade(int id_poi, DateTime data_i, DateTime data_f)
        {
            var mets = from meteo in db.Meteo
                       join poi in db.POI on meteo.LocalID equals poi.LocalID
                       where poi.ID == id_poi
                       && (meteo.dataLeitura == data_i || meteo.dataLeitura == data_f)
                       select new HumidadeDTO()
                       {
                           humidade = meteo.humidade,
                           data = meteo.dataLeitura
                       };
            return mets.AsQueryable();
        }

        /*
        var poi = db.POI.Find(id_poi);
            var lstMeteo = db.Meteo.Where(m => m.LocalID == poi.LocalID && m.dataLeitura >= data_i && m.dataLeitura <= data_f)
                                   .GroupBy(m => m.dataLeitura)
                                   .Select(g =>
                                   new HumidadeDTO
                                   {
                                       humidade = g.Average(m => m.humidade),
                                       data = g.Key
                                   });
            return lstMeteo;
    
        */


        //[Route("api/getTemperatura?idLocal={idLocal={idLocal}&data_i={data_i}&data_f={data_f}")]
        [Route("api/allpoi")]
        [HttpGet]
        public IQueryable<POIDTO> getAllPOI()
        {
            var mets = from poi in db.POI
                       join local in db.Locais on poi.LocalID equals local.ID
                       select new POIDTO()
                       {
                           ID = poi.ID,
                           Nome = poi.Nome,
                           Descricao = poi.Descricao,
                           GPS_Lat = local.GPSLat,
                           GPS_Long = local.GPSLong,
                           Created=poi.Created
                       };
            return mets.AsQueryable();
        }

        // GET: api/PointOfInterests
        public IQueryable<PointOfInterest> GetPOI()
        {
            return db.POI;
        }

        // GET: api/PointOfInterests/5
        [ResponseType(typeof(PointOfInterest))]
        public async Task<IHttpActionResult> GetPointOfInterest(int id)
        {
            PointOfInterest pointOfInterest = await db.POI.FindAsync(id);
            if (pointOfInterest == null)
            {
                return NotFound();
            }

            return Ok(pointOfInterest);
        }

        // PUT: api/PointOfInterests/5
        [Authorize(Roles ="Editor")]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPointOfInterest(int id, PointOfInterest pointOfInterest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pointOfInterest.ID)
            {
                return BadRequest();
            }

            var createdPoi = db.POI.Where(i=>i.ID==id).Select(p=>p.Created).First();

            if (createdPoi != User.Identity.Name)
                return BadRequest("You don't created with Point of Interest");

            pointOfInterest.Created = createdPoi;
            db.Entry(pointOfInterest).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PointOfInterestExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PointOfInterests
        [Authorize(Roles ="Editor")]
        [ResponseType(typeof(PointOfInterest))]
        public async Task<IHttpActionResult> PostPointOfInterest(PointOfInterest pointOfInterest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (pointOfInterest.Created != User.Identity.Name)
            {
                return BadRequest("You don't created with Point of Interest");
            }

            db.POI.Add(pointOfInterest);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = pointOfInterest.ID }, pointOfInterest);
        }

        // DELETE: api/PointOfInterests/5
        [Authorize(Roles = "Editor")]
        [ResponseType(typeof(PointOfInterest))]
        public async Task<IHttpActionResult> DeletePointOfInterest(int id)
        {
            PointOfInterest pointOfInterest = await db.POI.FindAsync(id);
            if (pointOfInterest == null)
            {
                return NotFound();
            }
            if (pointOfInterest.Created != User.Identity.Name)
                return BadRequest("You don't created this Point of Interest");
            db.POI.Remove(pointOfInterest);
            await db.SaveChangesAsync();

            return Ok(pointOfInterest);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PointOfInterestExists(int id)
        {
            return db.POI.Count(e => e.ID == id) > 0;
        }
    }
}