﻿using System.Data.Entity;
using ClassLibrary.Models;

namespace Datum
{
    public class DatumContext : DbContext 
    {
        public DatumContext() : base("Datum")
        {
        }

        public DbSet<Local> Locais { get; set; }

        public DbSet<Meteorologia> Meteo { get; set; }

        public DbSet<PointOfInterest> POI { get; set; }
    }
}
