﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ClassLibrary.Models
{
    public class Meteorologia
    {
        public int ID { get; set; }
        public int LocalID { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime dataLeitura { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:H:mm}")]
        public DateTime horaLeitura { get; set; }
        public double temp { get; set; }
        public double vento { get; set; }
        public double humidade { get; set; }
        public double pressao { get; set; }
        public double NO { get; set; }
        public double NO2 { get; set; }
        public double CO2 { get; set; }

        public Local Local { get; set; }
    }
}
