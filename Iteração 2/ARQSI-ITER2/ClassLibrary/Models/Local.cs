﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Models
{
    public class Local
    {
     
        public int ID { get; set; }
        public string Nome { get; set; }
        public string GPSLat { get; set; }
        public string GPSLong { get; set; }

        public ICollection<PointOfInterest> PointsOfInterest { get; set; }
        public ICollection<Meteorologia> Meteorologias { get; set; }
    }
}
