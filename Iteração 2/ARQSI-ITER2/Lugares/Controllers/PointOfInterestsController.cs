﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClassLibrary.Models;
using Datum;

namespace Lugares.Controllers
{
    [Authorize]
    [RequireHttps]
    public class PointOfInterestsController : Controller
    {
        private DatumContext db = new DatumContext();

        // GET: PointOfInterests
        public ActionResult Index()
        {
            var pOI = db.POI.Include(p => p.Local);
            return View(pOI.ToList());
        }

        // GET: PointOfInterests/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PointOfInterest pointOfInterest = db.POI.Find(id);
            if (pointOfInterest == null)
            {
                return HttpNotFound();
            }
            return View(pointOfInterest);
        }

        // GET: PointOfInterests/Create
        public ActionResult Create()
        {
            ViewBag.LocalID = new SelectList(db.Locais, "ID", "Nome");
            return View();
        }

        // POST: PointOfInterests/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Nome,Descricao,LocalID")] PointOfInterest pointOfInterest)
        {
            if (ModelState.IsValid)
            {
                pointOfInterest.Created = User.Identity.Name;
                db.POI.Add(pointOfInterest);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.LocalID = new SelectList(db.Locais, "ID", "Nome", pointOfInterest.LocalID);
            return View(pointOfInterest);
        }

        // GET: PointOfInterests/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PointOfInterest pointOfInterest = db.POI.Find(id);
            if (pointOfInterest == null)
            {
                return HttpNotFound();
            }
            if (pointOfInterest.Created != User.Identity.Name)
            {
                return Content("You don't created this Point Of Interest");
            }
            ViewBag.LocalID = new SelectList(db.Locais, "ID", "Nome", pointOfInterest.LocalID);
            return View(pointOfInterest);
        }

        // POST: PointOfInterests/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Nome,Descricao,LocalID")] PointOfInterest pointOfInterest)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pointOfInterest).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            if (pointOfInterest.Created != User.Identity.Name)
            {
                return Content("You don't created this Point Of Interest");
            }
            ViewBag.LocalID = new SelectList(db.Locais, "ID", "Nome", pointOfInterest.LocalID);
            return View(pointOfInterest);
        }

        // GET: PointOfInterests/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PointOfInterest pointOfInterest = db.POI.Find(id);
            if (pointOfInterest == null)
            {
                return HttpNotFound();
            }
            if (pointOfInterest.Created != User.Identity.Name)
            {
                return Content("You don't created this Point Of Interest");
            }
            return View(pointOfInterest);
        }

        // POST: PointOfInterests/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PointOfInterest pointOfInterest = db.POI.Find(id);
            db.POI.Remove(pointOfInterest);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
