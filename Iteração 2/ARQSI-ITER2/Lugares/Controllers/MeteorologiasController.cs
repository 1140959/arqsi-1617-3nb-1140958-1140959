﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClassLibrary.Models;
using Datum;

namespace Lugares.Controllers
{
    [Authorize]
    [RequireHttps]
    public class MeteorologiasController : Controller
    {
        private DatumContext db = new DatumContext();

        // GET: Meteorologias
        public ActionResult Index()
        {
            var meteo = db.Meteo.Include(m => m.Local);
            return View(meteo.ToList());
        }

        // GET: Meteorologias/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Meteorologia meteorologia = db.Meteo.Find(id);
            if (meteorologia == null)
            {
                return HttpNotFound();
            }
            return View(meteorologia);
        }

        // GET: Meteorologias/Create
        public ActionResult Create()
        {
            ViewBag.LocalID = new SelectList(db.Locais, "ID", "Nome");
            return View();
        }

        // POST: Meteorologias/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,LocalID,dataLeitura,horaLeitura,temp,vento,humidade,pressao,NO,NO2,CO2")] Meteorologia meteorologia)
        {
            if (ModelState.IsValid)
            {
                db.Meteo.Add(meteorologia);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.LocalID = new SelectList(db.Locais, "ID", "Nome", meteorologia.LocalID);
            return View(meteorologia);
        }

        // GET: Meteorologias/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Meteorologia meteorologia = db.Meteo.Find(id);
            if (meteorologia == null)
            {
                return HttpNotFound();
            }
            ViewBag.LocalID = new SelectList(db.Locais, "ID", "Nome", meteorologia.LocalID);
            return View(meteorologia);
        }

        // POST: Meteorologias/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,LocalID,dataLeitura,horaLeitura,temp,vento,humidade,pressao,NO,NO2,CO2")] Meteorologia meteorologia)
        {
            if (ModelState.IsValid)
            {
                db.Entry(meteorologia).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LocalID = new SelectList(db.Locais, "ID", "Nome", meteorologia.LocalID);
            return View(meteorologia);
        }

        // GET: Meteorologias/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Meteorologia meteorologia = db.Meteo.Find(id);
            if (meteorologia == null)
            {
                return HttpNotFound();
            }
            return View(meteorologia);
        }

        // POST: Meteorologias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Meteorologia meteorologia = db.Meteo.Find(id);
            db.Meteo.Remove(meteorologia);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
