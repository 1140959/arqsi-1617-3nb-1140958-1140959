﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;
using Visita.Helpers;
using Visita.Models.DTO;
using Newtonsoft.Json;
using Visita.Models;
using System;

namespace Visita.Controllers
{
    [Authorize]
    public class VisitaController : Controller
    {
        // GET: Consulta
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> PoIs()
        {
            HttpClient cancela = WebApiHttpClient.GetClient(); ;
            string url = "api/PointOfInterests";

            IList<POIDTO> list = new List<POIDTO>();
            HttpResponseMessage response = await cancela.GetAsync(url);

            if (response.IsSuccessStatusCode)
            {
                var data = await response.Content.ReadAsStringAsync();
                list = Newtonsoft.Json.JsonConvert.DeserializeObject<IList<POIDTO>>(data);

                ViewData["Pois"] = list;
                ViewBag.Pois = new SelectList(list, "ID", "Nome");
                ViewBag.Warning = "";
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> PoIs([Bind(Include = "poiID,Data_Inicio,Data_Fim,Hora_Inicio, Hora_Fim")]ConsultaDados consulta)
        {
            HttpClient cancela = WebApiHttpClient.GetClient();
            //"api/DadosMeteo/Pesquisa?id_poi={0}&data_i={1}&data_f={2}"
            string url = string.Format("api/DadosMeteo/Pesquisa?id_poi={0}",
                consulta.poiID);

            IEnumerable<MeteoDetailsDTO> list = new List<MeteoDetailsDTO>();
            HttpResponseMessage response = await cancela.GetAsync(url);

            if (response.IsSuccessStatusCode)
            {
                var dataMeteo = await response.Content.ReadAsStringAsync();
                list = Newtonsoft.Json.JsonConvert.DeserializeObject<IEnumerable<MeteoDetailsDTO>>(dataMeteo);
            }

            if (list == null)
            {
                TempData["Resultados"] = new List<MeteoDetailsDTO>();
            }
            else
            {
                IList<MeteoDetailsDTO> list2 = new List<MeteoDetailsDTO>();
                foreach (MeteoDetailsDTO dto in list)
                {
                    if (dto.dataLeitura >= consulta.Data_Inicio && dto.dataLeitura <= consulta.Data_Fim)
                        if (TimeSpan.Compare(dto.horaLeitura.TimeOfDay, consulta.Hora_Inicio.TimeOfDay) >= 0 && TimeSpan.Compare(dto.horaLeitura.TimeOfDay, consulta.Hora_Fim.TimeOfDay) <= 0)
                            list2.Add(dto);

                }
                TempData["Resultados"] = (IEnumerable<MeteoDetailsDTO>)list2;
            }

            return RedirectToAction("Details");
        }

        public ActionResult Details()
        {
            return View(TempData["Resultados"]);
        }

    }
}