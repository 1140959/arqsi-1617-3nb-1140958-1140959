﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Visita.Models.DTO
{
    public class POIDTO
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string Created { get; set; }
        //public Local Local { get; set; }
    }

    public class POIDetailsDto
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        //public Local Local { get; set; }
    }
}