﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Visita.Models.DTO
{
    public class MeteoDTO
    {
        public int ID { get; set; }
        //public Local Local { get; set; }
    }

    public class MeteoDetailsDTO
    {
        [Display(Name = "Ponto de Interesse")]
        public int ID { get; set; }

        [Display(Name = "Data de Inicio")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime dataLeitura { get; set; }

        [Display(Name = "Hora de Fim")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime horaLeitura { get; set; }

        [Display(Name = "Temperatura")]
        public double temp { get; set; }
        [Display(Name = "Vento")]
        public double vento { get; set; }
        [Display(Name = "Humidade")]
        public double humidade { get; set; }
        [Display(Name = "Pressao")]
        public double pressao { get; set; }
        [Display(Name = "NO")]
        public double NO { get; set; }
        [Display(Name = "NO2")]
        public double NO2 { get; set; }
        [Display(Name = "CO2")]
        public double CO2 { get; set; }
    }
}