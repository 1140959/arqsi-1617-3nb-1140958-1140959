﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Visita.Models
{
    public class ConsultaDados
    {
        [Display(Name = "Ponto de Interesse")]
        [Required(ErrorMessage = "Escolha o POI")]
        public int poiID { get; set; }

        [Display(Name = "Data de Inicio")]
        [Required(ErrorMessage = "Por favor insira a data")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Data_Inicio { get; set; }

        [Display(Name = "Data de Fim")]
        [Required(ErrorMessage = "Por favor insira a data")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Data_Fim { get; set; }

        [Display(Name = "Hora de Inicio")]
        [Required(ErrorMessage = "Por favor insira a hora")]
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime Hora_Inicio { get; set; }

        [Display(Name = "Hora de Inicio")]
        [Required(ErrorMessage = "Por favor insira a hora")]
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime Hora_Fim { get; set; }
    }
}
