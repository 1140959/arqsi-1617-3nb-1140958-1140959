var server = "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/";

function createXmlHttpRequestObject() {
    try {
        return new XMLHttpRequest();
    } catch (e) {}
    try {
        return new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {}
    alert("XMLHttpRequest nao suportado");
    return null;
}

function makeXmlHttpGetCall(url, params, async, xml, callback, args) {
    console.log(server+url);
    var xmlHttpObj = createXmlHttpRequestObject();
    if (xmlHttpObj) {
        xmlHttpObj.open("Get", server+url, async);
        xmlHttpObj.onreadystatechange = function () {
            if (xmlHttpObj.readyState == 4 && xmlHttpObj.status == 200) {
                var response;
                if (xml == true){
                    response = xmlHttpObj.responseXML;
                }
                else
                    response = xmlHttpObj.responseText;
                callback(response, args);
            }
        };
        xmlHttpObj.send(params);
    }
} 

function getSensoresAJAX() {
    var url = "sensores.php";
    makeXmlHttpGetCall(url, null, true, true, getSensoresRequest, []);
}

function getFacetasAJAX(id, nome) {
    var url = "facetas.php?sensor=" + id;
    makeXmlHttpGetCall(url, null, true, true, getFacetasRequest, [nome]);
}

function getResultadosAJAX() {
    url = generateSearchUrl();
    makeXmlHttpGetCall(url, null, true, false, getFacetasResultados);
}

function getSensoresData(response) {
    var sensores = [];
    var sensoresXml = response.getElementsByTagName("sensor");
    var i;
    for (i = 0; i < sensoresXml.length; i++) {
        sensores[i] = [];
        sensores[i][0] = sensoresXml[i].getElementsByTagName("nome")[0].childNodes[0].nodeValue;
        sensores[i][1] = sensoresXml[i].getElementsByTagName("descricao")[0].childNodes[0].nodeValue;
        sensores[i][2] = sensoresXml[i].getAttribute("id");
    }
    return sensores;
}

function getFacetasData(response) {
    var facetas = [];
    var facetasXml = response.getElementsByTagName("Faceta");
    var i;
    for (i = 0; i < facetasXml.length; i++) {
        facetas[i] = [];
        facetas[i][0] = facetasXml[i].getAttribute("id");
        facetas[i][1] = facetasXml[i].getElementsByTagName("CampoBD")[0].childNodes[0].nodeValue;
        facetas[i][2] = facetasXml[i].getElementsByTagName("Nome")[0].childNodes[0].nodeValue;
        facetas[i][3] = facetasXml[i].getElementsByTagName("Grandeza")[0].childNodes[0].nodeValue;
        facetas[i][4] = facetasXml[i].getElementsByTagName("Tipo")[0].childNodes[0].nodeValue;
        facetas[i][5] = facetasXml[i].getElementsByTagName("Semantica")[0].childNodes[0].nodeValue;
    }
    return facetas;
}