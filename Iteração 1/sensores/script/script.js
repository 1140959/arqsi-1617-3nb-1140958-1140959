function clearDiv(div) {
    while (div.hasChildNodes()) {
        div.removeChild(div.firstChild);
    }
}

function setAttributes(label, attri, append) {
    var label = document.createElement(label);
    var key;
    for (key in attri) {
        label.setAttribute(key, attri[key]);
    }
    if (append != null)
        label.appendChild(document.createTextNode(append));
    return label;
}

function setElements(element, attri) {
    var elements = document.getElementById(element);
    var key;
    for (key in attri) {
        elements.setAttribute(key, attri[key]);
    }
    return elements;
}

function setInputSensores(sensores) {
    var attriInput = {
        "id": sensores[0],
        "type": "radio",
        "name": "tab-group",
        "checked": "checked",
        "active": "0",
        "onclick": "getFacetasAJAX('" + sensores[2] + "','" + sensores[0] + "')"
    };
    var input = setAttributes("input", attriInput);
    return input;
}

function getSensoresRequest(response, args) {
    var sensores = getSensoresData(response);
    var div_sensores = document.getElementById("sensors");
    var i;
    for (i = 0; i < sensores.length; i++) {


        var input = setInputSensores(sensores[i]);
        var attri = {
            "for": sensores[i][0]
        };
        var label = setAttributes("label", attri, sensores[i][0]);

        div_sensores.appendChild(input);
        div_sensores.appendChild(label);
    }
}

function getFacetasRequest(response, args) {
    document.body.style.width = "100%";

    var divAtrib = {
        "class": "right",
        "value": args[0]
    };
    var div_facetas = setElements("selected_sensor", divAtrib);
    clearDiv(div_facetas);
    var facetas = getFacetasData(response);

    var labelAtrib = {
        "for": "resul",
        "class": "label"
    };
    var labelBtn = setAttributes("label", labelAtrib, "Pesquisa por Facetas");

    var tabAtrib = {
        "id": "pesquisa",
        "style": ""
    };

    var tab = setAttributes("div", tabAtrib);
    tab.appendChild(labelBtn);
    tab.appendChild(document.createElement("br"));
    tab.appendChild(document.createElement("br"));

    var i;
    for (i = 0; i < facetas.length; i++) {
        setFaceta(facetas[i], args, tab);
    }

    var bt = setAttributes("div", {
        "id": "img"
    });
    var bt_hide = setAttributes("input", {
        "type": "image",
        "src": "img/hide1.png"
    });
    bt_hide.onclick = function () {
        toggle_img(this, "pesquisa");
    }
    bt.appendChild(bt_hide);

    var btResAtrib = {
        "classe": "btn",
        "type": "button",
        "onclick": "getResultadosAJAX()"
    };

    var bt_resultados = setAttributes("button", btResAtrib, "Ver Resultados");

    tab.appendChild(bt_resultados);
    div_facetas.appendChild(tab);
    div_facetas.appendChild(bt);
}

function setInputFaceta(faceta, args) {
    var inputAtrib = {
        "type": "checkbox",
        "name": args[0],
        "grandeza": faceta[3],
        "tipo": faceta[4],
        "semantica": faceta[5],
        "value": faceta[2],
        "active": "1",
        "onclick": "toggle_faceta('" + args[0] + "','" + faceta[1] + "','" + faceta[3] + "','" + faceta[5] + "')"
    };
    var input = setAttributes("input", inputAtrib);
    return input;
}

function setFaceta(faceta, args, tab) {
    var id = faceta[2];

    var input = setInputFaceta(faceta, args);

    //TESTE FACETA TOGGLE
    var facetaValue = setAttributes("div", {
        "id": faceta[1],
        "name": faceta[1]
    });
    var sensor = document.getElementById("selected_sensor").getAttribute("value");
    facetaValue.style.display = "none";
    tab.appendChild(input);

    //<label for="id_of_checkbox">Text</label>
    var label = setAttributes("label", {
        "for": faceta[2]
    }, faceta[2]);
    tab.appendChild(label);

    tab.appendChild(facetaValue);
    tab.appendChild(document.createElement("br"));
}

function toggle_faceta(sensor, faceta_nome, grandeza, semantica) {
    var faceta = document.getElementById(faceta_nome);
    if (faceta.style.display == "block") {
        faceta.style.display = "none";
        faceta.setAttribute("active", "0");
    } else if (faceta.style.display == "none") {
        faceta.style.display = "block";
        faceta.setAttribute("active", "1");
        clearDiv(faceta);
        getFacetaWidgetAJAX(faceta_nome, grandeza, semantica, sensor);
    }
}

function toggle_img(btn, div) {
    var element = document.getElementById(div);
    if (btn.src.indexOf('show1.png') != -1) {
        btn.src = 'img/hide1.png';
        element.style.display = "block";
        document.getElementById("img").appendChild(btn);
    } else {
        btn.src = 'img/show1.png';
        element.style.display = "none";
        document.getElementById("selected_sensor").appendChild(btn);
    }
}

//incompleto
function filterDadosContinuos(dados) {
    var facetas = []; // get facetas continuas ativas
    var i, j, values;
    for (i = 0; i < facetas.length; i++) {
        if ("slider")
            values = getSliderValues("teste");
        else
            values = getDateValues();

        for (j = 0; j < dados.length; j++) {
            if (dados[j][facetas['id']] >= values['min'] && dados[j][facetas['id']] <= values['min'])
                delete dados[j];
        }
    }

    return dados;
}

function getDateValues() {
    var dates = [];
    dates[0] = $("#datepicker").datepicker().val();
    dates[1] = $("#datepicker2").datepicker().val();
    return dates;
}

function getSliderValues(faceta_nome) {
    return $('#' + faceta_nome + "_slider").slider("values");
}

function getFacetasResultados(response, args) {
    var sensor = document.getElementById("selected_sensor").getAttribute("value");
    if (args == null) {
        var url = "facetasByNomeSensor.php?sensor=" + sensor;
        makeXmlHttpGetCall(url, null, true, true, getFacetasResultados, [response]);
    } else {
        var facetas = getFacetasData(response)
        var dados = filterDadosContinuos(JSON.parse(args[0]));
        var table = buildTableContent(facetas, dados);
        if (facetas.length > 7)
            document.body.style.width = "150%";
        document.getElementById("pesquisa").appendChild(buildTable(table));
    }
}

function buildTable(table) {
    var div_table = setAttributes("div", {
        "id": "table"
    });

    $("#resultados").remove();
    var resultAtrib = {
        "id": "resultados",
        "class": "result"
    };
    var result = setAttributes("div", resultAtrib);

    var labelBtnAtrib = {
        "for": "resul",
        "class": "label"
    };
    var labelBtn = setAttributes("label", labelBtnAtrib, "Resultados");
    result.appendChild(document.createElement("br"));
    result.appendChild(labelBtn);
    result.appendChild(document.createElement("br"));
    result.appendChild(document.createElement("br"));

    div_table.appendChild(table);
    result.appendChild(div_table);
    return result;
}

function buildTableContent(facetas, dados) {
    var table = document.createElement("table");
    table.setAttribute("border", "1");
    //Table Header
    var array = [],
        td, i, j;
    var tr = document.createElement("tr");
    for (i = 0; i < facetas.length; i++) {
        td = document.createElement("td");
        array[i] = facetas[i][1];
        td.appendChild(document.createTextNode(facetas[i][1]));
        tr.appendChild(td);
    }
    table.appendChild(tr);
    //table content
    for (j = 0; j < dados.length; j++) {
        tr = document.createElement("tr");
        for (i = 0; i < array.length; i++) {
            td = document.createElement("td");
            var t = dados[j][array[i]];
            td.appendChild(document.createTextNode(t));
            tr.appendChild(td);
        }
        table.appendChild(tr);
    }
    return table;
}

function generateSearchUrl() {
    var sensor = document.getElementById("selected_sensor").getAttribute("value");
    var url = "valoresDeSensor.php?sensor=" + sensor;
    var facetas = document.getElementById('pesquisa').getElementsByTagName("input");
    var j;
    //debugger;
    for (j = 0; j < facetas.length; j++) {
        if (facetas[j].getAttribute("active") == 1 && facetas[j].getAttribute("grandeza") == "discreto")
            url += generateFacetaUrl(facetas[j]);
    }
    return url;
}

function generateFacetaUrl(faceta) {
    var nome = faceta.getAttribute("value"),
        url = "",
        k = 0,
        j;
    if (faceta.getAttribute("type") == "checkbox") { //type="checkbox"
        if (document.getElementById(nome).getAttribute("active")) {
            url += "&" + nome + "=[";

            var inputs = document.getElementById(nome).getElementsByTagName("input");
            for (j = 0; j < inputs.length; j++) {
                var input = inputs[j];
                if (input.getAttribute("active") == 1) {
                    if (k > 0)
                        url += ",";
                    url += input.getAttribute("value");
                    k++;
                }
            }
            url += "]";
        }
    } else { // combobox
        var e = document.getElementById(nome + "_selected");
        var str = e.options[e.selectedIndex].text;
        url += "&" + nome + "=" + str;
    }
    return url;
}