function getFacetaWidgetAJAX(faceta_nome, grandeza, semantica, sensor) {
    var faceta = document.getElementById(faceta_nome);
    var url_cont = "maxFaceta.php?sensor=" + sensor + "&facetaCont=" + faceta_nome;
    var url_disc = "valoresFacetadoSensor.php?sensor=" + sensor + "&faceta=" + faceta_nome;
    switch (semantica) {
        case "data":
            makeXmlHttpGetCall(url_cont, null, true, false, buildDatePicker, [faceta_nome]);
            break;
        case "hora":
            makeXmlHttpGetCall(url_cont, null, true, false, buildTimePicker, [sensor, faceta_nome]);
            break;
        case "temperatura":
        case "coordenadas":
        case "monetário":
        case "quantidade":
            makeXmlHttpGetCall(url_cont, null, true, false, buildSlider, [sensor, faceta_nome]);
            break;
        case "localização":
        case "qualidade":
        case "foto":
            makeXmlHttpGetCall(url_disc, null, true, false, buildCheckbox, [faceta_nome]);
            break;
        case "organização":
            makeXmlHttpGetCall(url_disc, null, true, false, buildComboBox, [faceta_nome]);
            break;
        default:
            break;
    }
}

function buildSlider(response, args) {
    var dados = JSON.parse(response);
    var url = "minFaceta.php?sensor=" + args[0] + "&facetaCont=" + args[1];
    makeXmlHttpGetCall(url, null, true, false, buildSlider2, [dados["max"], args[1]]);
}

function buildSlider2(response, args) {
    var faceta = document.getElementById(args[1]);
    var dados = JSON.parse(response);
    var slider_id = args[1] + "_slider";
    var input_id = args[1] + "_input";

    var div = document.createElement("div");
    div.setAttribute("id", slider_id);
    div.setAttribute("min", args[0]);
    div.setAttribute("max", dados["min"]);

    var input = document.createElement("input");
    input.setAttribute("type", "text");
    input.setAttribute("id", input_id);
    input.setAttribute("style", "border:0;");
    input.setAttribute("readonly", "");

    faceta.appendChild(div);
    faceta.appendChild(input);

    slider(slider_id, input_id, args[0], dados["min"]);
}

function slider(slider_id, input_id, maxi, mini) {
    $(function () {
        console.log(maxi + " " + mini);
        if (maxi == null || maxi == "") {
            maxi = 0;
        }
        if (mini == null || mini == "") {
            mini = 0;
        }
        maxi = parseFloat(maxi);
        mini = parseFloat(mini);

        $("#" + slider_id).slider({
            range: true,
            min: mini,
            max: maxi,
            step: 0.01,
            values: [mini, maxi],
            slide: function (event, ui) {
                $("#" + input_id).val(ui.values[0] + " - " + ui.values[1]);
            }
        });
        $("#" + input_id).val($("#" + slider_id).slider("values", 0) +
            " - " + $("#" + slider_id).slider("values", 1));
    });
}

function buildCheckbox(response, args) {
    var faceta = document.getElementById(args[0]);
    var dados = JSON.parse(response);
    var i;
    for (i = 0; i < dados.length; i++) {
        var check = document.createElement("input");
        check.setAttribute("type", "checkbox");
        check.setAttribute("active", "0");
        check.setAttribute("value", dados[i]);
        check.onclick = function () {
            if (this.getAttribute("active") == 0) {
                this.setAttribute("active", "1");
            } else {
                this.setAttribute("active", "0");
            }
        }

        var tab = document.createElement("tab");
        check.setAttribute("style", "margin-left:2em");
        tab.appendChild(document.createTextNode(dados[i]));

        faceta.appendChild(check);
        faceta.appendChild(tab)
        faceta.appendChild(document.createElement("br"));
    }
}

function buildDatePicker(response, args) {
    var faceta = document.getElementById(args[0]);
    var p = document.createElement("p");
    var input_de = document.createElement("input");
    input_de.setAttribute("type", "text");
    input_de.setAttribute("id", "datepicker");

    p.appendChild(document.createTextNode("De: "));
    p.appendChild(input_de);
    p.appendChild(document.createElement("br"));

    var input_ate = document.createElement("input");
    input_ate.setAttribute("type", "text");
    input_ate.setAttribute("id", "datepicker2");
    p.appendChild(document.createTextNode("Até: "));
    p.appendChild(input_ate);

    faceta.appendChild(p);
    $("#datepicker").datepicker({
        dateFormat: "yy-mm-dd"
    });
    $("#datepicker2").datepicker({
        dateFormat: "yy-mm-dd"
    });
}

function buildTimePicker(response, args) {
    //substituir por http://jsfiddle.net/jrweinb/MQ6VT/
    var faceta = document.getElementById(args[1]);
    var dados = JSON.parse(response);
    var horas = ["0:00", "1:00", "2:00", "3:00", "4:00", "5:00", "6:00", "7:00", "8:00", "9:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"];
    var select = document.createElement("select");
    select.setAttribute("id", args[1] + "_selected")
    var i;
    for (i = 0; i < horas.length; i++) {
        var option = document.createElement("option");
        option.setAttribute("value", horas[i]);
        option.appendChild(document.createTextNode(horas[i]));
        select.appendChild(option);
    }
    faceta.appendChild(select);
}

function buildComboBox(response, args) {
    var faceta = document.getElementById(args[0]);
    var dados = JSON.parse(response);
    var select = document.createElement("select");
    select.setAttribute("id", args[0] + "_selected")
    var i;
    for (i = 0; i < dados.length; i++) {
        var option = document.createElement("option");
        option.setAttribute("value", dados[i]);
        option.appendChild(document.createTextNode(dados[i]));
        select.appendChild(option);
    }
    faceta.appendChild(select);
}