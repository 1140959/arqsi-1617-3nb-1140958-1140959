function loadWidgets() {
    widget_vertical();
}

function widget_vertical() {
    var div = document.getElementById("widget_central");
    clearDiv(div);

    var iframe = document.createElement("iframe");
    iframe.setAttribute("src", "sensores/sensor.html");
    //iframe.setAttribute("width", "100%");
    iframe.setAttribute("width", "515");
    iframe.setAttribute("style", "height: 100vh;");
    div.appendChild(iframe);
    //<iframe src="widget.html" width="515" style="height: 100vh;"></iframe>
}

function clearDiv(div) {
    while (div.hasChildNodes()) {
        div.removeChild(div.firstChild);
    }
}